<?php

/**
 * @file
 * This is the file description for Webform Auto Field module.
 *
 * This module adds a textarea to the webform creation form to enter a list of
 * field titles. It also adds an additional field to select textfield or
 * textarea generation. This list is used to automatically generate fields for
 * each one of the items in the list. This saves you a lot of time if you need
 * to generate multiple fields of a particular type for a long form.
 */

/**
 * Implements hook_perm().
 */
function webform_auto_field_perm() {
  return array('administer webform auto field');
}

/**
 * Implements hook_form_alter().
 *
 * Modify the node/add/webform form to add the fieldset to handle which type of
 * field to generate and the textarea to handle the list of field names.
 */
function webform_auto_field_form_webform_node_form_alter(&$form, $form_state) {
  $form['automatic_field_generation'] = array(
    '#type' => 'fieldset',
    '#title' => t('Automatic Field Generation'),
    '#description' => t('Automatically generate fields from a list of field titles.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#access' => user_access('administer webform auto field'),
  );

  $form['automatic_field_generation']['type_of_field'] = array(
    '#type' => 'select',
    '#title' => t('Type of Field'),
    '#description' => t('Select the type of field you want to automatically generate.'),
    '#options' => array('textfield' => 'textfield', 'textarea' => 'textarea'),
    '#default_value' => 'textfield',
  );

  $form['automatic_field_generation']['field_titles'] = array(
    '#type' => 'textarea',
    '#title' => t('Field Titles'),
    '#description' => t('Enter a list of field titles. A field of the type you selected above will be created for each one of the title you enter here. You may enter a comma seperated list or enter one on each line.'),
  );
}

/**
 * Implements hook_nodeapi().
 *
 * When the webform is being created iterate through the list of field titles
 * and create a field for each one.
 */
function webform_auto_field_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {
  if ($node->type == 'webform') {
    switch ($op) {
      case 'insert':
      case 'update':
        _webform_auto_field_add_fields($node);
        break;
    }
  }
}

/**
 * Add the auto-generated fields.
 *
 * @param $node
 *   The node object.
 *
 * @return
 *   TRUE unless the node field_titles are empty.
 */
function _webform_auto_field_add_fields($node) {
  if (empty($node->field_titles)) {
    return FALSE;
  }

  $fixed_titles = str_replace(', ', ',', $node->field_titles);
  $fixed_titles = str_replace("\r\n", ',', $fixed_titles);
  $title_array = explode(',', $fixed_titles);
  $weight = 0;

  foreach ($title_array as $field_title) {
    $field_title = check_plain(trim($field_title));
    $field_title_key = str_replace(' ', '_', strtolower($field_title));

    $field_form_state = array(
      'submitted' => TRUE,
      'values' => array(
        'type' => $node->type_of_field,
        'nid' => $node->nid,
        'clone' => FALSE,
        'name' => $field_title,
        'form_key' => $field_title_key,
        'weight' => $weight,
        'op' => 'Submit',
      ),
    );

    $field_component = array(
      'type' => $node->type_of_field,
      'nid' => $node->nid,
      'name' => $field_title,
      'weight' => $weight,
      'mandatory' => 0,
    );

    drupal_execute('webform_component_edit_form', $field_form_state, $node, $field_component);
    $weight++;
  }
  return TRUE;
}
